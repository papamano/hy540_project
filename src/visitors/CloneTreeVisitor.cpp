#include "CloneTreeVisitor.h"

#include "HiddenTags.h"
#include "PostOrderTreeHost.h"
#include "SetParentTreeVisitor.h"
#include "TreeTags.h"

#include <cassert>

void CloneTreeVisitor::LeafClone(const Object &node) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::SingleChildClone(const Object &node, const std::string &childType) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    /* An empty statement or a signle semicolon do not have any child nodes */
    if (node[childType])
        clone->Set((childType), *(node[(childType)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::DoubleChildClone(const Object &node, const std::string &childType1, const std::string &childType2) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    clone->Set((childType1), *(node[(childType1)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType2), *(node[(childType2)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::MultiChildClone(const Object &node) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    for (register unsigned i = 0; i < clone->GetNumericSize(); ++i) {
        clone->Set(i, *(node[i]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    }
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::TripleChildIfClone(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    clone->Set((childType1), *(node[(childType1)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType2), *(node[(childType2)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    if (node.ElementExists(childType3))
        clone->Set((childType3), *(node[(childType3)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::CloneFunctionDef(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    clone->Set((childType1), *(node[(childType1)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType2), *(node[(childType2)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType3), *(node[(childType3)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

void CloneTreeVisitor::CloneFor(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3, const std::string &childType4) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    clone->Set((childType1), *(node[(childType1)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType2), *(node[(childType2)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType3), *(node[(childType3)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    clone->Set((childType4), *(node[(childType4)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}
void CloneTreeVisitor::CloneReturn(const Object &node, const std::string &childType) {
    Object *clone = node.Clone();
    clone->RemoveIfExist(PARENT_RESERVED_FIELD);
    if (node.ElementExists(childType))
        clone->Set((childType), *(node[(childType)]->ToObject_NoConst()->GetAndRemove(CLONED_TREE_RESERVED_FIELD)));
    const_cast<Object &>(node).Set(CLONED_TREE_RESERVED_FIELD, Value(clone));
    result = &node;
}

Object *CloneTreeVisitor::GetResult(void) {
    assert(result);
    Object *clone = const_cast<Object *>(result)->GetAndRemove(CLONED_TREE_RESERVED_FIELD)->ToObject_NoConst();
    PostOrderTreeHost setParentTreeHost(new SetParentTreeVisitor());
    setParentTreeHost.Accept(*clone);
    return clone;
}

void CloneTreeVisitor::VisitProgram(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitStatements(const Object &node) { MultiChildClone(node); }
void CloneTreeVisitor::VisitStatement(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitExpression(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitAssign(const Object &node) { DoubleChildClone(node, AST_TAG_LVALUE, AST_TAG_RVALUE); }
void CloneTreeVisitor::VisitPlus(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitMinus(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitMul(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitDiv(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitModulo(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitGreater(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitLess(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitGreaterEqual(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitLessEqual(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitEqual(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitNotEqual(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitAnd(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitOr(const Object &node) { DoubleChildClone(node, AST_TAG_FIRST_EXPR, AST_TAG_SECOND_EXPR); }
void CloneTreeVisitor::VisitTerm(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitUnaryMinus(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitNot(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitPlusPlusBefore(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitPlusPlusAfter(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMinusMinusBefore(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMinusMinusAfter(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitPrimary(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMetaParse(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMetaUnparse(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMetaInline(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMetaEscape(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitMetaQuasiQuotes(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitRuntimeEmpty(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitLValue(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitId(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitLocal(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitDoubleColon(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitDollar(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitDollarEnv(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitDollarLambda(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitMember(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitDot(const Object &node) { DoubleChildClone(node, AST_TAG_LVALUE, AST_TAG_ID); }
void CloneTreeVisitor::VisitBracket(const Object &node) { DoubleChildClone(node, AST_TAG_LVALUE, AST_TAG_EXPR); }
void CloneTreeVisitor::VisitCall(const Object &node) { TripleChildIfClone(node, AST_TAG_FUNCTION, AST_TAG_ARGUMENTS, AST_TAG_LVALUE); }
void CloneTreeVisitor::VisitArgumentList(const Object &node) { MultiChildClone(node); }
void CloneTreeVisitor::VisitNamedArgument(const Object &node) { DoubleChildClone(node, AST_TAG_NAMED_KEY, AST_TAG_NAMED_VALUE); }
void CloneTreeVisitor::VisitExpressionList(const Object &node) { MultiChildClone(node); }
void CloneTreeVisitor::VisitObjectDef(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitIndexed(const Object &node) { MultiChildClone(node); }
void CloneTreeVisitor::VisitIndexedElem(const Object &node) { DoubleChildClone(node, AST_TAG_OBJECT_KEY, AST_TAG_OBJECT_VALUE); }
void CloneTreeVisitor::VisitBlock(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitFunctionDef(const Object &node) { CloneFunctionDef(node, AST_TAG_FUNCTION_ID, AST_TAG_FUNCTION_FORMALS, AST_TAG_STMT); }
void CloneTreeVisitor::VisitConst(const Object &node) { SingleChildClone(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitNumber(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitString(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitNil(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitTrue(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitFalse(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitIdList(const Object &node) { MultiChildClone(node); }
void CloneTreeVisitor::VisitFormal(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitIf(const Object &node) { TripleChildIfClone(node, AST_TAG_CONDITION, AST_TAG_STMT, AST_TAG_ELSE_STMT); }
void CloneTreeVisitor::VisitWhile(const Object &node) { DoubleChildClone(node, AST_TAG_CONDITION, AST_TAG_STMT); }
void CloneTreeVisitor::VisitFor(const Object &node) { CloneFor(node, AST_TAG_FOR_PRE_ELIST, AST_TAG_CONDITION, AST_TAG_FOR_POST_ELIST, AST_TAG_STMT); }
void CloneTreeVisitor::VisitReturn(const Object &node) { CloneReturn(node, AST_TAG_CHILD); }
void CloneTreeVisitor::VisitBreak(const Object &node) { LeafClone(node); }
void CloneTreeVisitor::VisitContinue(const Object &node) { LeafClone(node); }
