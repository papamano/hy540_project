#include "Deallocator.h"
#include "IncreaseRefCountVisitor.h"
#include "Interpreter.h"
#include "PostOrderTreeHost.h"
#include "SetParentTreeVisitor.h"
#include "UnparseVisitor.h"
#include "Utilities.h"
#include "ValidityVisitor.h"
#include "VisualizeVisitor.h"
#include "parser.h"

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <signal.h>

constexpr unsigned DEFAULT_STACK_SIZE = (64 * 1024 * 1024);

#define PREPROCESSOR_TEMP_FILE_1 ".tmp1.tmp"
#define PREPROCESSOR_TEMP_FILE_2 ".tmp2.tmp"

#define PREPROCESSOR_SHORT_FLAG "-p"
#define PREPROCESSOR_LONG_FLAG "--preprocessor"

#define PREPROCESSOR_GENERATED_FILE ".preprocessed.alpha"

class Application {
private:
    PostOrderTreeHost* host = nullptr;
    PostOrderTreeHost* validityHost = nullptr;

    IncreaseRefCounterVisitor *incrRefCounterVisitor = new IncreaseRefCounterVisitor();
    UnparseVisitor *unparseVisitor = new UnparseVisitor();
    VisualizeVisitor *visualizeVisitor = new VisualizeVisitor("alpha_OriginalAST.dot");
    SetParentTreeVisitor *setParentTreeVisitor = new SetParentTreeVisitor();
    ValidityVisitor *validityVisitor = new ValidityVisitor();

    Interpreter *interpreter = nullptr;

public:
    Application(void) {
        interpreter = Interpreter::GetInstance();
        host = new PostOrderTreeHost({
            incrRefCounterVisitor,
            unparseVisitor,
            visualizeVisitor,
            setParentTreeVisitor,
        });
        validityHost = new PostOrderTreeHost(validityVisitor);
    }

    void Usage(const std::string &str) {
        std::cout << "Usage: " << (str.empty() ? "interpreter.out" : str) << " [OPTIONS] [input_file]\n"
                  << std::endl
                  << "Options:" << std::endl
                  << "    -p, --preprocessor        Use C preprocessor" << std::endl
                  << "    input_file                The file containing the source code to be compiled" << std::endl;
    }

    void ProcessAST(Object *ast) {
        assert(ast && ast->IsValid());

        interpreter->SetAnonymousFuncOffset(GetAnonymousFuncsOffset());

        host->Accept(*ast);
        validityHost->Accept(*ast);

        unparseVisitor->WriteFile();
        visualizeVisitor->WriteFile();
        interpreter->Execute(*ast);

#ifdef AST_MEM_CLEANUP
        PostOrderTreeHost *h = new PostOrderTreeHost(new DeallocateVisitor());
        h->Accept(*ast);
        delete h;
#endif
    }

    bool IsSanitized(const std::string & str) {
        return ((str.find(";") == std::string::npos) &&
                (str.find("*") == std::string::npos) &&
                (str.find("&&") == std::string::npos) &&
                (str.find("||") == std::string::npos) &&
                (str.find("$") == std::string::npos) &&
                (str.find("(") == std::string::npos) &&
                (str.find(")") == std::string::npos) &&
                (str.find("-") == std::string::npos));
    }

    bool FileExists(const std::string & filename) {
        FILE * file = nullptr;
        if (!(file = fopen(filename.c_str(), "r"))) return false;
        fclose(file);
        return true;
    }

    std::string CreatePreprocessCommand(const std::string & filename, const std::string & newFilename) {
        std::stringstream ss;

        /* The code that will be executed using the preprocessor */
        ss << "cpp -undef -E -P -nostdinc 2> /dev/null " << filename
           << " > " << PREPROCESSOR_TEMP_FILE_1 << "; "
           /* The code that will be executed without the preprocessor (simply
            * remove the comments) */
           << "cpp -P -fpreprocessed -dD -E 2> /dev/null " << filename
           << " > " << PREPROCESSOR_TEMP_FILE_2 << "; "
           /* If these two files are the same then that means that no
            * preprocessor directive was used so we can simply use the original
            * file */
           << " if ! grep -vxFf " << PREPROCESSOR_TEMP_FILE_1 << " " << PREPROCESSOR_TEMP_FILE_2 << " > /dev/null; "
           << "then cp " << filename << " " << newFilename << "; "
           /* Otherwise let the preprocessor do its magic and then use the
            * generated file */
           << "else cpp -undef -P -nostdinc -E -CC 2> /dev/null " << filename
           << " > " + newFilename << "; "
           << "fi; ";

        return ss.str();
    }

    std::string PreprocessFile(const std::string & filename) {
        assert(!filename.empty());

        std::string newFilename = PREPROCESSOR_GENERATED_FILE;

        if (!IsSanitized(filename)) {
            std::cerr << "Illegal character in filename. Code injection detected" << std::endl;
            exit(EXIT_FAILURE);
        }

        if (!FileExists(filename)) {
            std::cerr << "Cannot open file \"" << filename << "\"" << std::endl;
            exit(EXIT_FAILURE);
        }

        int result = std::system(CreatePreprocessCommand(filename, newFilename).c_str());

        if(result == -1) {
            std::cerr << "Preprocessor execution failed" << std::endl;
            exit(EXIT_FAILURE);
        }

        std::remove(PREPROCESSOR_TEMP_FILE_1);
        std::remove(PREPROCESSOR_TEMP_FILE_2);

        return newFilename;
    }

    int Run(int argc, char **argv) {
        Object *ast = nullptr;
        bool preprocessor = false;
        std::string filename = "";

        if (argc > 3) {
            Usage(argv[0]);
            return EXIT_FAILURE;
        }

        if (argc == 3) {
            std::string arg1(argv[1]);
            std::string arg2(argv[2]);

            if (arg1 == PREPROCESSOR_SHORT_FLAG || arg1 == PREPROCESSOR_LONG_FLAG) {
                preprocessor = true;
                filename = arg2;
            }
            else if (arg2 == PREPROCESSOR_SHORT_FLAG || arg2 == PREPROCESSOR_LONG_FLAG) {
                preprocessor = true;
                filename = arg1;
            }
            else {
                Usage(argv[0]);
                return EXIT_FAILURE;
            }
        }

        if (argc == 2) filename = std::string(argv[1]);

        if (argc < 2)
            ast = ParseStdin();
        else if (preprocessor) {
            std::string targetFile = PreprocessFile(filename);
            ast = ParseFile(targetFile);
            std::remove(targetFile.c_str());
        } else {
            ast = ParseFile(filename);
        }

        /* Could not open file */
        if (!ast) return EXIT_FAILURE;

        ProcessAST(ast);

        return EXIT_SUCCESS;
    }

    ~Application() {
        delete host;
        delete validityHost;
        delete interpreter;
    }
};

void SignalHandler(int signal) {
    if (signal == SIGSEGV) {
        std::cerr << "An unexpected error occur.\nExiting..." << std::endl;
        exit(EXIT_FAILURE);
    } else assert(false);
}

int main(int argc, char **argv) {
    try {
#undef SEGFAULT_MASK
#ifdef SEGFAULT_MASK
        signal(SIGSEGV, SignalHandler);
#endif

        Utilities::SetStackSize(DEFAULT_STACK_SIZE);

        Application app;
        return app.Run(argc, argv);

    } catch (std::exception &e) {
        std::cerr << "Unhandled exception: " << e.what() << std::endl;
        return (EXIT_FAILURE);
    }

    /* No man's land */
    assert(false);
    return (EXIT_FAILURE);
}
