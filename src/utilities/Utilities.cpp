#include "Utilities.h"

#include <sys/resource.h>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>

bool Utilities::IsZero(double num) {
    unsigned val = static_cast<unsigned>(num * 100000.0f);
    return val == 0;
}

bool Utilities::IsInt(double num) {
    double flooredValue = std::floor(std::abs(num));
    double epsilon = std::numeric_limits<double>::epsilon();
    return (std::abs(num - flooredValue) < epsilon);
}

bool Utilities::DoublesAreEqual(double a, double b) {
    double epsilon = std::numeric_limits<double>::epsilon();
    return std::fabs(a - b) < epsilon;
}

std::string Utilities::UnparserFormatEscChars(const std::string &str) {
    std::string out;
    for (const auto &c : str) {
        if (c == '\n')
            out += "\\n";
        else if (c == '\t')
            out += "\\t";
        else if (c == '\"')
            out += "\\\"";
        else if (c == '\\')
            out += "\\\\";
        else
            out += c;
    }
    return out;
}

void Utilities::SyntaxError(const std::string &msg, unsigned line) {
    std::string lineMsg = (line != 0) ? (" at line " + std::to_string(line)) : "";

    std::cerr << RED_COLOR
              << "Syntax Error" << lineMsg << ": "
              << NO_COLOR << msg << std::endl;
    exit(EXIT_FAILURE);
}

void Utilities::SyntaxWarning(const std::string &msg, unsigned line) {
    std::string lineMsg = (line != 0) ? (" at line " + std::to_string(line)) : "";

    std::cerr << YELLOW_COLOR
              << "Syntax Warning" << lineMsg << ": "
              << NO_COLOR << msg << std::endl;
}

std::string Utilities::TrimString(const std::string & s) {
    std::string str = s;

    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) {
                  return !std::isspace(ch);
              }));

    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) {
                  return !std::isspace(ch);
              }).base(),
              str.end());

    return str;
}

void Utilities::SetStackSize(unsigned size) {
    const rlim_t stackSize = size;
    struct rlimit limit;
    int result;

    result = getrlimit(RLIMIT_STACK, &limit);
    if (result == 0) {
        if (limit.rlim_cur < stackSize) {
            limit.rlim_cur = stackSize;
            result = setrlimit(RLIMIT_STACK, &limit);

            if (result != 0) {
                std::cerr << "Failed to set stack size limit. Error code: " << result << std::endl;
                fprintf(stderr, "setrlimit returned result = %d\n", result);
            }
        }
    }
}