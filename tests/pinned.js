//Not segmentation
function outer(x) {
    function inner() {
        y = 5;
        print(x);
    }

    return inner;
}

call = outer(10);
call();

//BlockAssertCrash.alpha still crashes on assertion, not runtime error
