/* x is now an object that represents the following AST (simplified):
 *
 *    program
 *       |
 *      stmt
 *       |
 *      expr
 *       |
 *       +
 *    /     \
 *   /       \
 * const    const
 *   |        |
 *   1        2
 */
x = @( "1 + 2;" );

// Represents a list of items that can be used as an argument list
y = <<1, true, "str", [1, 2]>>;

// Represents the AST of the entire for loop
z = <<
    for(i = 0; i < 5; ++i) print(i + 1);
>>;

!( z );

// Represents the AST of the entire function
u = <<
    function add(x,y)
        { return x + y; }
>>;

/* Function has not been declared. The code has not been executed/evaluated. We
 * simply have its AST */
assert($env.add == nil);

// Now the function can be used
!( u );
print(add(2, 4));

// A nested AST value (AST representing an AST)
w = << << 1 + 2 >> >>;

assert(typeof( !( w ) ) == "object");

ast = << print(7); print(8); >>;
biggerAST = << ~ast; print(9); >>;
!( biggerAST );

x = <<1>>;
y = <<~x + 2>>;                     // y is <<1 + 2>>
z = <<~x + ~y * 3>>;

print( !( z ) );

print( !( << 11, 12, 13 >> ) );

args = << "Hello", "World" >>;

call1 = <<print(~args)>>;
call2 = <<print("Before", ~args, "After")>>;
call3 = << <<print(~args)>> >>;

!( call1 );
!( call2 );
!( !( call3 ) );

content = << "value", "text", "number", x >>;
x = "foobar";
object = [ !( content ) ];
print(object);

function one() { !(<<return 1;>>); }
print(one());

statements = [
    << print(10); >>,
    << x = 0; x = 1; print(x); >>,
    << for(i = 0; i < 3; ++i) print(i); >>,
    << cond = true and false; if (cond) print("Error"); else print("yeah!"); >>
];

code = << print("All the following statements are executed using metaprogramming"); >>;
print("I will execute the following code:");
for(i = 0; i < object_size(statements); ++i) {
    print( #( statements[i] ) );
    code = << ~code; ~( statements[i] ); >>;
}

!(code);

/* Runtime error here. You can not write: var = stmt */
y = !(<<for(i = 0; i < 1; ++i) print(i); >> );