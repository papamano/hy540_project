/*
 * The point of this test program is to find different ways to declare variables
 * using strings for their names.
 * The point here would be to read the variable name from stdin and declare the
 * variables that the user wants
 */

/* Function that tries to declare a variable with the given name and value */
function DeclareVariable(name, value = undef()) {
    assert(typeof(name) == "string");
    assert(typeof(value) != "nil");

    $env.$outer[name] = value;
}

globalVariable = false;

print("Function");

{
    DeclareVariable("x");
    DeclareVariable("y", 30);

    /* Declared variables are not in the correct scope because a function was
     * used. User's code does not have access in scope stack.
     * Expected is nill for both variables */
    print($env.x);
    print($env.y);
}

print("\nDirect scope access");

{
    variablesToDeclare = [ "X", "Y", "Z", "W", "F" ];

    /* A more clever way is to get access to the current scope directly and
     * plant the variable there. */
    for(i = 0; i < object_size(variablesToDeclare); ++i)
        $env[variablesToDeclare[i]] = i;

    print($env.X);
    print($env.Y);
    print($env.Z);
    print($env.W);
    print($env.F);

    /* The problem here is that this method declares the variable as local. It
     * does not perform a symbol lookup. As a result, access to existing
     * variables in other scopes is not possible */
    $env["globalVariable"] = true;
    print(::globalVariable); // Does not print true :(

    $env.globalVariable = true;
    print(::globalVariable); // Does not print true :(
}

/* An alternative way to declare variables from strings in the correct scope is
 * using metaprogramming */

print("\nMetaprogramming");

/* This could be:
 * varName = input();
 * varValue = input();
 */
varName = "foo";
varValue = "\"" + "value" + "\""; // Pay attention to quotes

{
    // Declare a variable called foobar with value 50
    !( @( "foobar" + "=" + "50" + ";" ));

    !( @( varName + " = " + varValue + ";" ) );

    print($env.foobar);
    print($env.foo);

    /* Using metaprogramming we can emulate both local and simple IDs */
    varName = "globalVariable";
    !( @( varName + " = true;" ) );
    print(::globalVariable); // Yeah! It prints true! :D

    /* However the problem with this method is that we cannot use it in for
     * loops because the ! operator will be removed after 1 execution */
    variablesToDeclare = [ "X", "Y", "Z"];
    for(i = 0; i < object_size(variablesToDeclare); ++i)
        !( @( variablesToDeclare[i] + " = " + to_string(i) + ";" ) );

    /* We exepct only X to have a value because the for loop will be changed to
     * x = 0; after the first iteration */
    sprint("X has a value: ", $env.X != nil, "\n");
    sprint("Y has a value: ", $env.Y != nil, "\n");
    sprint("Z has a value: ", $env.Z != nil, "\n");

    /* We can solve this by creating the code in the loop and parsing/executing
     * it after the loop */
    code = "";
    for(i = 0; i < object_size(variablesToDeclare); ++i)
        code = code + variablesToDeclare[i] + " = " + to_string(i) + ";";
    !( @( code ) );

    sprint("X has a value: ", $env.X != nil, "\n");
    sprint("Y has a value: ", $env.Y != nil, "\n");
    sprint("Z has a value: ", $env.Z != nil, "\n");
}

print(globalVariable); // Make sure that the correct variable was modified