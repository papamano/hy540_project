print("Function Generator:\n");

function FunctionGenerator(body) {
    assert(typeof(body) == "object");

    /* It is necessary that the generator returns an AST so that in every call
     * the body of the function changes */
    return << ( function() { ~body; } ) >>;
}

f = !( FunctionGenerator( << print("Metaprogramming is the best!!!"); >> ) );
f();

f = !( FunctionGenerator( << print("[Thor] Is it though?"); >> ) );
f();

g = !( FunctionGenerator( <<
    print("I am a function that was generated using meta programming. Everything else is the same");

    x = 0;
    print($env.x);
    print($lambda);

    print("I can even have variadic arguments");
    for(i = 0; $env[i] != nil; ++i) sprint($env[i], " ");
    sprint("\n");

>> ) );


g("Hello", "Metaprogramming", "World");


print("\n\nObject Factories:\n");

function Factory() {
    /* We want even number of arguments but the current environment always
     * contains the field $outer that should be considered */
    assert(object_size($env) % 2 == 1, "Only even number of arguments is allowed");
    args = $env;

    code = << local object = []; >>;

    for(i = 0; args[i] != nil; i = i + 2) {
        /* Ensure that both arguments are ASTs. We should validate them but
         * there is no appropriate library function. The best thing we can do is
         * to ensure that they are objects */
        assert(typeof(args[i]) == "object");
        assert(typeof(args[i + 1]) == "object");

        index = args[i];
        value = args[i + 1];
        code = << ~code; object[ ~index ] = ~value; >>;
    }

    code = << ~code; $lambda.$closure.$local = object; return object; >>;

    /* We could simply use the function generator from the previous example */
    return << (function() {
        ~code;
    }) >>;
}

PersonFactory = !( Factory(<< "name" >>, << "John Doe" >>, << "age" >>, << 10 + 11 >> ) );

person1 = PersonFactory();
print(person1);

person2 = PersonFactory();
person2.name = person2.name + " Unknown";
person2.age = person1.age + 2;
print(person2);

AnimalFactory = !( Factory(<< "species" >>, << "not registered" >>, << "kills" >>, << 10 >>, << "printer" >>, << print >>, << "evolve" >>, << ( function() { print("Formation of new species. Previous kills "); } ) >>) );
print(AnimalFactory());

StudentFactory = !( Factory( << "name" >>, << "Jane Public" >>,
                             << "grade" >>, << 0 >>,
                             << "Print" >>, << ( function() {
                                /* Function work as methods and automatically
                                 * have access to fields of objects */
                                sprint("My name is ", name, " and my grade is ", grade, "\n");
                            } ) >> ) );

student1 = StudentFactory();
student1.grade = 10;
student1.Print();