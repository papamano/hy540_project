#ifndef _CLONE_TREE_VISITOR_H_
#define _CLONE_TREE_VISITOR_H_

#include "TreeVisitor.h"

class CloneTreeVisitor final : public TreeVisitor {
private:
    const Object *result;

    void LeafClone(const Object &node);
    void SingleChildClone(const Object &node, const std::string &childType);
    void DoubleChildClone(const Object &node, const std::string &childType1, const std::string &childType2);
    void MultiChildClone(const Object &node);
    void TripleChildIfClone(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3);
    void CloneFunctionDef(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3);
    void CloneFor(const Object &node, const std::string &childType1, const std::string &childType2, const std::string &childType3, const std::string &childType4);
    void CloneReturn(const Object &node, const std::string &childType);

public:
    CloneTreeVisitor() : result(nullptr) {}
    virtual ~CloneTreeVisitor(){};
    TreeVisitor *Clone(void) const override { return new CloneTreeVisitor(); };
    Object *GetResult(void);

    void VisitProgram(const Object &node) override;
    void VisitStatements(const Object &node) override;
    void VisitStatement(const Object &node) override;
    void VisitExpression(const Object &node) override;
    void VisitAssign(const Object &node) override;
    void VisitPlus(const Object &node) override;
    void VisitMinus(const Object &node) override;
    void VisitMul(const Object &node) override;
    void VisitDiv(const Object &node) override;
    void VisitModulo(const Object &node) override;
    void VisitGreater(const Object &node) override;
    void VisitLess(const Object &node) override;
    void VisitGreaterEqual(const Object &node) override;
    void VisitLessEqual(const Object &node) override;
    void VisitEqual(const Object &node) override;
    void VisitNotEqual(const Object &node) override;
    void VisitAnd(const Object &node) override;
    void VisitOr(const Object &node) override;
    void VisitTerm(const Object &node) override;
    void VisitUnaryMinus(const Object &node) override;
    void VisitNot(const Object &node) override;
    void VisitPlusPlusBefore(const Object &node) override;
    void VisitPlusPlusAfter(const Object &node) override;
    void VisitMinusMinusBefore(const Object &node) override;
    void VisitMinusMinusAfter(const Object &node) override;
    void VisitPrimary(const Object &node) override;
    void VisitMetaParse(const Object &node) override;
    void VisitMetaUnparse(const Object &node) override;
    void VisitMetaInline(const Object &node) override;
    void VisitMetaEscape(const Object &node) override;
    void VisitMetaQuasiQuotes(const Object &node) override;
    void VisitRuntimeEmpty(const Object &node) override;
    void VisitLValue(const Object &node) override;
    void VisitId(const Object &node) override;
    void VisitLocal(const Object &node) override;
    void VisitDoubleColon(const Object &node) override;
    void VisitDollar(const Object &node) override;
    void VisitDollarEnv(const Object &node) override;
    void VisitDollarLambda(const Object &node) override;
    void VisitMember(const Object &node) override;
    void VisitDot(const Object &node) override;
    void VisitBracket(const Object &node) override;
    void VisitCall(const Object &node) override;
    void VisitArgumentList(const Object &node) override;
    void VisitNamedArgument(const Object &node) override;
    void VisitExpressionList(const Object &node) override;
    void VisitObjectDef(const Object &node) override;
    void VisitIndexed(const Object &node) override;
    void VisitIndexedElem(const Object &node) override;
    void VisitBlock(const Object &node) override;
    void VisitFunctionDef(const Object &node) override;
    void VisitConst(const Object &node) override;
    void VisitNumber(const Object &node) override;
    void VisitString(const Object &node) override;
    void VisitNil(const Object &node) override;
    void VisitTrue(const Object &node) override;
    void VisitFalse(const Object &node) override;
    void VisitIdList(const Object &node) override;
    void VisitFormal(const Object &node) override;
    void VisitIf(const Object &node) override;
    void VisitWhile(const Object &node) override;
    void VisitFor(const Object &node) override;
    void VisitReturn(const Object &node) override;
    void VisitBreak(const Object &node) override;
    void VisitContinue(const Object &node) override;
};
#endif
